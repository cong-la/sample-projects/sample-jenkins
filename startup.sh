echo "Testing startup.sh"
cat > sample_text.txt
for i in 0 1 2 3 4 5 6 7 8 9
do 
    echo "Running for loop i = $i" # This PRINTS to the screen but does not save to file.
    echo "Running for loop i = $i" >> sample_text.txt # This SAVES to file but does not print on screen.
done
echo "This is sample text." >> sample_text.txt
echo "read from file"
cat sample_text.txt

python3 startup.py